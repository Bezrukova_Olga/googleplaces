package com.sight.converter;

import com.sight.dto.*;
import com.sight.model.EntityCity;
import com.sight.model.EntityCountry;
import com.sight.model.EntitySight;
import com.sight.model.EntityStatus;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class Converter {
    public EntitySight toEntity(RequestSight sight) {
        return new EntitySight();
    }

    public ResponseSight toResponse(EntitySight sight) {
        return new ResponseSight();
    }

    public EntityCountry fromRequestCountryToEntity(RequestCountry country) {
        return new EntityCountry(country.getName());
    }

    public ResponseCountry fromEntityCountryToResponse(EntityCountry country) {
        return new ResponseCountry(country.getId(), country.getName());
    }

    public EntityCity fromRequestCityToEntity(RequestCity city) {
        return new EntityCity(city.getName(), city.getIdCountry(), city.getLatitude(), city.getLongitude());
    }

    public EntityStatus fromRequestStatusToEntity(RequestStatus status) {
        return new EntityStatus(status.getName());
    }

    public List<RequestCountry> fromFileToRequestCountry() {
        List<RequestCountry> listCountry = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("output.csv"))) {
            String line = reader.readLine();
            String checkString = "";
            while ((line = reader.readLine()) != null) {
                String[] readLine = line.split(";");
                if (!(checkString.equals(readLine[1]))) {
                    checkString = readLine[1];
                    RequestCountry country = new RequestCountry(readLine[1]);
                    listCountry.add(country);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listCountry;
    }

    public List<RequestCity> fromFileToRequestCity() {
        List<RequestCity> listCity = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("output.csv"))) {
            String line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] readLine = line.split(";");
                RequestCity city = new RequestCity(readLine[3], null, readLine[7], readLine[8]);
                listCity.add(city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listCity;
    }

    public List<RequestStatus> fromFileToRequestStatus() {
        List<RequestStatus> listStatus = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("status.csv"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                RequestStatus status = new RequestStatus(line);
                listStatus.add(status);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listStatus;
    }



}
