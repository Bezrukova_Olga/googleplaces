package com.sight.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "country")
public class EntityCountry {
    @Id
    private String id;
    private String name;

    @OneToMany(mappedBy="idCountry")
    private Set<EntityCity> citySet;

    public EntityCountry() {
    }

    public EntityCountry(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
