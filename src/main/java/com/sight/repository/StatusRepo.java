package com.sight.repository;

import com.sight.model.EntityStatus;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepo extends CrudRepository<EntityStatus, String> {
}
