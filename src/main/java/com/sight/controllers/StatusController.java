package com.sight.controllers;

import com.sight.converter.Converter;
import com.sight.dto.RequestCity;
import com.sight.dto.RequestStatus;
import com.sight.model.EntityCity;
import com.sight.model.EntityStatus;
import com.sight.repository.CityRepo;
import com.sight.repository.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/status")
public class StatusController {
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private Converter converter;

    @RequestMapping(method = RequestMethod.GET)
    public HttpStatus addStatus() {
        List<RequestStatus> listStatus = converter.fromFileToRequestStatus();
        for (RequestStatus element : listStatus) {
            EntityStatus statusEntity = converter.fromRequestStatusToEntity(element);
            statusRepo.save(statusEntity);
        }
        return HttpStatus.OK;
    }
}
