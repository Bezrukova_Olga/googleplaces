package com.sight.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "monument")
public class EntitySight {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name="city_id", referencedColumnName="id")
    private EntityCity idCity;
    private String name;
    private String latitude;
    private String longitude;
    @ManyToOne
    @JoinColumn(name="status__ID", referencedColumnName="id")
    private EntityStatus idStatus;

    public EntitySight() {
    }

    public EntitySight(EntityCity idCity, String name, String latitude, String longitude, EntityStatus idStatus) {
        this.id = UUID.randomUUID().toString();
        this.idCity = idCity;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.idStatus = idStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EntityCity getIdCity() {
        return idCity;
    }

    public void setIdCity(EntityCity idCity) {
        this.idCity = idCity;
    }

    public void setIdStatus(EntityStatus idStatus) {
        this.idStatus = idStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "EntitySight{" +
                "id='" + id + '\'' +
                ", idCity=" + idCity +
                ", name='" + name + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", idStatus=" + idStatus +
                '}';
    }
}
