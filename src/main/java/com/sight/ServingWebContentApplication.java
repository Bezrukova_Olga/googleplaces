package com.sight;


import com.sight.utils.LogicPlace;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServingWebContentApplication {
    public static void main(String[] args){
        SpringApplication.run(ServingWebContentApplication.class, args);
        new LogicPlace().logic();
//        GooglePlacesClient newClient = new GooglePlacesClient();
//        newClient.getSight("25.2048493","55.2707828","museum");
    }
}
