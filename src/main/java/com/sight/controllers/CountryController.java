package com.sight.controllers;

import com.sight.converter.Converter;
import com.sight.dto.RequestCountry;
import com.sight.dto.ResponseCountry;
import com.sight.model.EntityCountry;
import com.sight.repository.CountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/countries")
public class CountryController {
    @Autowired
    private CountryRepo countryRepo;
    @Autowired
    private Converter converter;

    @RequestMapping(method = RequestMethod.GET)
    public HttpStatus addCountry() {
        List<RequestCountry> listCountry = converter.fromFileToRequestCountry();
        for (RequestCountry element : listCountry) {
            EntityCountry countryEntity = converter.fromRequestCountryToEntity(element);
            countryRepo.save(countryEntity);
        }
        return HttpStatus.OK;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseCountry getCountry(@PathVariable String id) {
        EntityCountry found = countryRepo.findById(id).get();
        return converter.fromEntityCountryToResponse(found);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteCountry(@PathVariable String id) {
        countryRepo.deleteById(id);
        return HttpStatus.OK;
    }
}
