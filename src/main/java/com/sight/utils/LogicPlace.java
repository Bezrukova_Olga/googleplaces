package com.sight.utils;

import com.sight.model.EntityCity;
import com.sight.repository.CityRepo;
import org.springframework.beans.factory.annotation.Autowired;

public class LogicPlace{

    @Autowired
    private CityRepo cityRepo;

    public void logic(){
        Iterable<EntityCity> cities = cityRepo.findAll();
        for (EntityCity city: cities)
            System.out.println(city.toString());
    }
}
