package com.sight.dto;

import java.util.UUID;

public class RequestStatus {
    private String id;
    private String name;

    public RequestStatus() {
    }

    public RequestStatus(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
