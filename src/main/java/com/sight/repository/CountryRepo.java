package com.sight.repository;

import com.sight.model.EntityCountry;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepo extends CrudRepository<EntityCountry, String> {
}
