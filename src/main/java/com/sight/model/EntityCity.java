package com.sight.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "city")
public class EntityCity {

    @Id
    private String id;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="country_id")
    private EntityCountry idCountry;
    private String latitude;
    private String longitude;

    public EntityCity() {
    }

    public EntityCity(String name, EntityCountry idCountry, String latitude, String longitude) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.idCountry = idCountry;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityCountry getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(EntityCountry idCountry) {
        this.idCountry = idCountry;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "EntityCity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", idCountry=" + idCountry +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
