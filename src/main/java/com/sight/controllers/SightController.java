package com.sight.controllers;

import com.sight.connection.Connection;
import com.sight.converter.Converter;
import com.sight.model.EntitySight;
import com.sight.dto.RequestSight;
import com.sight.dto.ResponseSight;
import com.sight.repository.SightRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sights")
public class SightController {
    @Autowired
    private SightRepo sightRepo;
    @Autowired
    private Converter converter;

    @RequestMapping(method = RequestMethod.GET)
    public HttpStatus addSight() {
        Connection connection = new Connection();
        List<EntitySight> list = connection.connect();
        for (EntitySight element : list) {
            sightRepo.save(element);
        }
        return HttpStatus.OK;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseSight getSight(@PathVariable String id) {
        EntitySight found = sightRepo.findById(id).get();
        return converter.toResponse(found);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteSight(@PathVariable String id) {
        sightRepo.deleteById(id);
        return HttpStatus.OK;
    }
}
