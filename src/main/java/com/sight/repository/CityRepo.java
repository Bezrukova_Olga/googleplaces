package com.sight.repository;

import com.sight.model.EntityCity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepo extends CrudRepository<EntityCity, String> {

}
