package com.sight.connection;


import com.sight.dto.ResponseCity;
import com.sight.model.EntityCity;
import com.sight.model.EntityCountry;
import com.sight.model.EntitySight;
import com.sight.model.EntityStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Connection {
    private DBWorker dbWorker = new DBWorker();
    private String queryCity = "select * from city";
    private String queryStatus = "select * from status_monument";
    private List<EntityStatus> listStatus = new ArrayList<>();

    private List<EntitySight> listSight = new ArrayList<>();

    public List<EntitySight> connect() {
        try {
            Statement statement = dbWorker.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(queryStatus);
            while (resultSet.next()){
                EntityStatus entityStatus = new EntityStatus(resultSet.getString("name"));
                entityStatus.setId(resultSet.getString("id"));
                listStatus.add(entityStatus);
            }

            resultSet = statement.executeQuery(queryCity);

            int count = 0;

            while (resultSet.next() && count < 3) {
                count++;
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                String latitude = resultSet.getString("latitude");
                String longitude = resultSet.getString("longitude");
                EntityCountry idCountry = (EntityCountry) resultSet.getObject("country_id");
                EntityCity city = new EntityCity(name, idCountry, latitude, longitude);
                city.setId(id);
                for(EntityStatus status: listStatus) {
                    HttpURLConnection connection = null;
                    try {
                        String query = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=5000&type="+status.getName()+"&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
                        connection = (HttpURLConnection) new URL(query).openConnection();
                        connection.setRequestMethod("GET");
                        connection.setUseCaches(false);
                        connection.connect();
                        StringBuilder sb = new StringBuilder();
                        if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            String line;
                            while ((line = in.readLine()) != null) {
                                sb.append(line);
                                sb.append("\n");
                            }
                        }
                        JSONObject json = new JSONObject(sb.toString());
                        JSONArray results = json.getJSONArray("results");
                        int i = 0;
                        while (i < results.length()) {
                            JSONObject subArray = results.getJSONObject(i);
                            JSONObject geometry = subArray.getJSONObject("geometry");
                            JSONObject location = geometry.getJSONObject("location");
                            String nameSight = subArray.getString("name");
                            EntitySight entitySight = new EntitySight(city, nameSight, location.getNumber("lat").toString(),
                                    location.getNumber("lng").toString(), status);
                            listSight.add(entitySight);
                            i++;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listSight;
    }

}
