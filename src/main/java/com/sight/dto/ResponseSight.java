package com.sight.dto;



public class ResponseSight {

    private String id;

    private String idCity;
    private String name;
    private String picture;
    private String latitude;
    private String longitude;

    private String idStatus;

    public ResponseSight() {
    }

    public ResponseSight(String id, String idCity, String name, String picture, String latitude, String longitude, String idStatus) {
        this.id = id;
        this.idCity = idCity;
        this.name = name;
        this.picture = picture;
        this.latitude = latitude;
        this.longitude = longitude;
        this.idStatus = idStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(String idStatus) {
        this.idStatus = idStatus;
    }
}
