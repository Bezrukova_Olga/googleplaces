package com.sight.dto;

import com.sight.model.EntityCountry;

public class ResponseCity {
    private String id;
    private String name;
    private EntityCountry idCountry;
    private String latitude;
    private String longitude;

    public ResponseCity() {
    }

    public ResponseCity(String id, String name, EntityCountry idCountry, String latitude, String longitude) {
        this.id = id;
        this.name = name;
        this.idCountry = idCountry;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityCountry getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(EntityCountry idCountry) {
        this.idCountry = idCountry;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "ResponseCity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", idCountry=" + idCountry +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
