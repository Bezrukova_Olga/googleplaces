package com.sight.controllers;

import com.sight.converter.Converter;
import com.sight.dto.RequestCity;
import com.sight.model.EntityCity;
import com.sight.repository.CityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@RestController
//@RequestMapping("/cities")
//public class CityController {
//    @Autowired
//    private CityRepo cityRepo;
//    @Autowired
//    private Converter converter;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public HttpStatus addCity() {
//        List<RequestCity> listCountry = converter.fromFileToRequestCity();
//        for (RequestCity element : listCountry) {
//            EntityCity cityEntity = converter.fromRequestCityToEntity(element);
//            cityRepo.save(cityEntity);
//        }
//        return HttpStatus.OK;
//    }
//    @RequestMapping(value = "/cityGet",method = RequestMethod.GET)
//    public HttpStatus getCity() {
//        Iterable<EntityCity> all = cityRepo.findAll();
//        for(EntityCity city: all){
//            System.out.println(city.toString());
//        }
//        return HttpStatus.OK;
//    }
//
//}
