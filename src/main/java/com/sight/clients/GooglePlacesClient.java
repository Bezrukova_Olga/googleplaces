package com.sight.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sight.dto.RequestSight;
import org.springframework.web.client.RestTemplate;

import java.util.List;


public class GooglePlacesClient {
    private List<RequestSight> listSight;
    public List<RequestSight> getSight(String latitude, String longitude, String statusName) throws JsonProcessingException {
        final String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude
                + "&radius=5000&type=" + statusName + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode results = mapper.readTree(forObject).get("results");
        if(results.isArray()){
            for(JsonNode node: results){
                JsonNode name = node.get("name");
                JsonNode location = node.get("geometry").get("location");
                RequestSight sight = new RequestSight(name.toString(), location.get("lat").toString(), location.get("lng").toString());
                listSight.add(sight);
            }
        }
        return listSight;
    }
}
