package com.sight.repository;

import com.sight.model.EntitySight;
import org.springframework.data.repository.CrudRepository;

public interface SightRepo extends CrudRepository<EntitySight, String> {
}
